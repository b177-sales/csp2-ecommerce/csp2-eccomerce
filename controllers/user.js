const User = require('../models/User');
const Products = require('../models/Products');
const Orders = require('../models/Orders');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNum: reqBody.mobileNum,
		password: bcrypt.hashSync(reqBody.password, 10),
	});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

module.exports.checkDuplicateEmail = (reqBody) => {
	return User.find({ email: reqBody.email }).then((result) => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		}
	});
};

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then((result) => {
		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = '';

		// Returns the user information with the password as an empty string
		return result;
	});
};

// User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password,
				result.password
			);

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) };
			} else {
				return Promise.resolve(`Incorrect Password. Sir Alvin is the Best.`);
			}
		}
	});
};

// Set user as admin (Admin only)
module.exports.setAsAdmin = (data) => {
	if (data.isAdmin) {
		let updateAdminField = {
			isAdmin: true,
		};
		return User.findByIdAndUpdate(data.userId, updateAdminField).then(
			(user, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			}
		);
	} else {
		return Promise.resolve(
			`Can not proceed pareh as user kasi is not an Admin -Jowanna Q.`
		);
	}
};

// Non-admin User checkout (Create Order)
module.exports.addProduct = async (data) => {
	if (data.isAdmin == false) {
		let isUserUpdated = await User.findById(data.userId).then((user) => {
			user.cart.push({ productId: data.productId });

			return user.save().then((user, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			});
		});
		let isProductUpdated = await Products.findById(data.productId).then(
			(product) => {
				product.orders.push({ userId: data.userId });

				return product.save().then((product, error) => {
					if (error) {
						return false;
					} else {
						return true;
					}
				});
			}
		);
		let isOrderUpdated = await Products.findById(data.productId).then(
			(product) => {
				let newOrder = new Orders({
					userId: data.userId,
					productId: data.productId,
					purchasedOn: new Date(),
				});
				return newOrder.save().then((product, error) => {
					if (error) {
						return false;
					} else {
						return true;
					}
				});
			}
		);
		if (isUserUpdated && isProductUpdated && isOrderUpdated) {
			return 'Order has been created';
		} else {
			return "Order can't be created";
		}
	} else {
		return Promise.resolve("Can't process order, check permission");
	}
};

// Retrieve all orders (Admin only)
module.exports.getOrders = (data) => {
	if (data.isAdmin) {
		return Orders.find({}).then((result) => {
			return result;
		});
	} else {
		return false;
	}
};

// Retrieve authenticated user’s orders
module.exports.getMyOrders = (data) => {
	return User.findById(data.userId).then((result) => {
		if (data.isAdmin == false) {
			return result;
		} else {
			return false;
		}
	});
};
